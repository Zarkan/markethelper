package markethelper

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/mpiannucci/peakdetect"
	stan "github.com/nats-io/go-nats-streaming"
)

type PeakData struct {
	*PairValues
	PeakHigh *bool
	PeakLow  *bool
	High     *int
	Low      *int
}

func GetData(market, pair, period string, maxRange int, peakDetect float64, duration time.Duration) (*[]int, *[]int, *[]PeakData) {

	var natsURL = "nats://nats.default:4222"
	sc, err := stan.Connect("test-cluster", "generator-ob-112311", stan.NatsURL(natsURL))

	if err != nil {
		log.Printf("Can't connect: %v.\nMake sure a NATS Streaming Server is running at: %s", err, natsURL)

	}
	defer sc.Close()
	data := make([]PeakData, 0, 100000)
	topic := fmt.Sprintf("marketData.%s.%s.%s", market, period, pair)
	messageChan := make(chan *PairValues, 100000)
	sub, err := sc.Subscribe(topic, func(msg *stan.Msg) {

		var decodedMsg PairValues
		if err := DecodeGob(msg, &decodedMsg); err != nil {
			log.Printf("Decode PV err: %v", err.Error())
		}
		//	log.Printf("Decode PV err: %v", decodedMsg)
		if decodedMsg.Name == pair {

			messageChan <- &decodedMsg
		}

	}, stan.StartAtTimeDelta(duration))
	if err != nil {
		log.Fatalf("ERROR: STAN WS: Subscribe error: %v", err.Error())
	}
	defer sub.Unsubscribe()
	c := time.After(time.Second * 20)

	for {
		select {
		case msg := <-messageChan:
			data = append(data, PeakData{PairValues: msg})

		case <-c:
			log.Printf("Train data: %v", len(data))
			close := make([]float64, len(data))
			for i, d := range data {
				close[i] = d.Value
			}
			mini, _, maxi, _ := peakdetect.PeakDetect(close[:], data[0].Value*peakDetect)
			log.Printf("max: %v min: %v", len(mini), len(maxi))

			for i := len(maxi) - 1; i >= 0; i-- {
				if maxi[i] <= maxRange {
					RemoveInt(&maxi, i)
				}
				if maxi[i] >= len(data)-1-maxRange {
					RemoveInt(&maxi, i)
				}
			}
			for i := len(mini) - 1; i >= 0; i-- {
				if mini[i] <= maxRange {
					RemoveInt(&mini, i)
				}
				if mini[i] >= len(data)-1-maxRange {
					RemoveInt(&mini, i)
				}
			}
			for j, _ := range maxi {
				h := len(mini) + j
				//	maxi[j] = maxi[j] - 1
				data[maxi[j]].High = &h
			}
			for j, _ := range mini {
				h := j
				//	mini[j] = mini[j] - 1
				data[mini[j]].Low = &h
			}
			//	WriteToFile(&data, "peak-raw")

			return &maxi, &mini, &data
		}
	}

}
func RemoveInt(p *[]int, i int) {
	s := *p
	s = append(s[:i], s[i+1:]...) // perfectly fine if i is the last element
	*p = s
}
func DecodeGob(m *stan.Msg, ret interface{}) error {
	buffer := bytes.NewBuffer(m.Data)
	dec := gob.NewDecoder(buffer)
	err := dec.Decode(ret)
	if err != nil {
		log.Printf("decode error: %v", err)
	}
	return err
}

func WriteToFile(g interface{}, name string) {
	log.Println("GeneratedData: WriteToFile")
	filePath := fmt.Sprintf("./generatedData/%s-%v", name, time.Now().Format("2006-01-02-150405"))

	file, err := os.Create(filePath)
	defer file.Close()
	if err == nil {
		encoder := gob.NewEncoder(file)
		encoder.Encode(g)
	} else {
		log.Println("error err:%v", err)
	}

	log.Printf("GeneratedData: WriteToFile Generated %v\n", filePath)

}

func LoadFromFile(path string, ref interface{}) error {
	var f *os.File
	var err error
	if f, err = os.Open(path); err != nil {
		log.Fatal(err)
		return err
	}
	defer f.Close()

	decoder := gob.NewDecoder(f)
	err = decoder.Decode(ref)
	return nil
}
