package markethelper

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/fxsjy/gonn/gonn"
)

type RBFGA struct {
	Rbf          *gonn.RBFNetwork
	PeaksIdx     []int
	PeakRange    int
	InputSize    int
	SpeciesPeaks []int
}

type GenConfiguration struct {
	Market     string
	Pair       string
	Src        string
	Dst        string
	Vector     Vector
	Indicators []string
}

type RBFGen struct {
	Market string
	Pair   string
	Src    string
	Dst    string
	Vector Vector
	Gen    *gonn.RBFNetwork
}

type State int

const ( // iota is reset to 0 ddd
	Unverified State = iota // c0 == 0
	Enabled
	Disabled
)

type Vector int

const ( // iota is reset to 0
	Raise Vector = iota // c0 == 0
	Drop
)

type Comperator int

const ( // iota is reset to 0
	Lower Comperator = iota // c0 == 0
	Greater
)

type RBF struct {
	ID           uint `gorm:"primary_key" json:",string"`
	CreatedAt    time.Time
	Src          string     `gorm:"column:src"`
	Dst          string     `gorm:"column:dst"`
	Pair         string     `gorm:"column:pair"`
	Market       string     `gorm:"column:market"`
	State        State      `gorm:"column:state"`
	Vector       Vector     `gorm:"column:vector"`
	Comperator   Comperator `gorm:"column:comperator"`
	CompareValue float64    `gorm:"column:compare_value;type:float"`
	Rbfgendata   []byte     `gorm:"column:rbfgen;type:blob"  json:"-"`
	RBFGen       *RBFGen    `gorm:"-" json:"-"`
}

func (rbf *RBF) LoadNN() error {

	buff := bytes.NewBuffer(rbf.Rbfgendata)

	dec := gob.NewDecoder(buff) // Will write to network.

	// Encode (send) some values.
	err := dec.Decode(&rbf.RBFGen)
	if err != nil {
		log.Printf("Decode NN: %v error: %v", rbf.ID, err.Error())
		return err
	}
	return nil
}

func (rbf *RBF) EncodeNN() error {

	var buf bytes.Buffer        // Stand-in for a network connection
	enc := gob.NewEncoder(&buf) // Will write to network.

	// Encode (send) some values.
	err := enc.Encode(&rbf.RBFGen)
	if err != nil {
		log.Printf("encode NN: %v error: %v", rbf.ID, err.Error())
		return err
	}
	rbf.Rbfgendata = buf.Bytes()

	return nil
}

func (g *RBFGen) WriteRBFToFile(name string) {
	log.Println("GeneratedData: WriteToFile")

	filePath := fmt.Sprintf("./generatedNetwork/rbfdump-%s-%v", name, time.Now().Format("2006-01-02-150405"))

	file, err := os.Create(filePath)
	defer file.Close()
	if err == nil {
		encoder := json.NewEncoder(file)
		encoder.Encode(g)
	} else {
		log.Println("error err:%v", err)
	}

	log.Printf("RBF GEN Data: WriteToFile Generated %v\n", filePath)

}

func LoadRBFGen(path string) *RBFGen {
	var f *os.File
	var err error
	if f, err = os.Open(path); err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	c := &RBFGen{}
	decoder := gob.NewDecoder(f)
	err = decoder.Decode(c)
	return c
}
