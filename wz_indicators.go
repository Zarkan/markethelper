package markethelper

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

type CapMessage struct {
	Indicators *map[string]float64
	Currency   string
	MarketCap  CurrencyCap
}
type CurrencyCap struct {
	ID               string  `json:"id"`
	Name             string  `json:"name"`
	Symbol           string  `json:"symbol"`
	Rank             float64 `json:"rank,string"`
	PriceUsd         float64 `json:"price_usd,string"`
	PriceBtc         float64 `json:"price_btc,string"`
	Two4HVolumeUsd   float64 `json:"24h_volume_usd,string"`
	MarketCapUsd     float64 `json:"market_cap_usd,string"`
	AvailableSupply  float64 `json:"available_supply,string"`
	TotalSupply      float64 `json:"total_supply,string"`
	PercentChange1H  float64 `json:"percent_change_1h,string"`
	PercentChange24H float64 `json:"percent_change_24h,string"`
	PercentChange7D  float64 `json:"percent_change_7d,string"`
	LastUpdated      float64 `json:"last_updated,string"`
}

func CalculateZRI(pv *PairValues, cap CurrencyCap) {
	(*pv.Indicators)["ZRI"] = cap.MarketCapUsd/cap.AvailableSupply - pv.Value
	(*pv.Indicators)["1HPC"] = cap.PercentChange1H
	(*pv.Indicators)["24HPC"] = cap.PercentChange24H
	(*pv.Indicators)["7DPC"] = cap.PercentChange7D

}

func CalculateZCI(pv *PairValues) {

}

var myClient = &http.Client{Timeout: 10 * time.Second}

func GetCurrencyCapUSDT() (*[]CurrencyCap, error) {

	endpoint := fmt.Sprintf(
		"https://api.coinmarketcap.com/v1/ticker/?convert=USDT",
	)
	var err error
	var r *http.Response
req_loop:
	for {
		r, err = myClient.Get(endpoint)
		if err != nil {

			log.Printf("Could not get market ticks: %v", err)
			timer := time.NewTimer(time.Millisecond * 200)
			<-timer.C

		} else {
			break req_loop
		}
	}
	defer r.Body.Close()

	var response []CurrencyCap
	err = json.NewDecoder(r.Body).Decode(&response)
	if err != nil {
		log.Printf("Can't Decode capMarkets")
		return nil, err
	}

	return &response, nil
}
