package markethelper

import (
	"fmt"
	"log"
	"math"
	"strings"
)

type PVPeriod int

const (
	MINUTE PVPeriod = iota
	FIVE_MIN
	THIRTY_MIN
	HOUR
	DAY
)

//PairValue - market/wartosc
type PairValues struct {
	Id                                 int `json:"id"`
	Period                             PVPeriod
	Market                             string
	Name                               string
	Value, LowestAsk, HighestBid, Open float64
	BaseVolume                         float64
	Time                               int64
	Indicators                         *map[string]float64
	Dohlcv                             *DOHLCV
}

func (p *PairValues) String() string {
	return fmt.Sprintf("%v: %f %v", p.Name, p.Value, p.Time)
}

func (p *PairValues) Verify(market string) {
	for key, val := range *p.Indicators {
		if math.IsNaN(val) || math.IsInf(val, 0) {
			log.Printf("Value: %s:%s :%s has NaN value\n", market, p.Name, key)
		}

	}

}

func (mp *PairValues) GetFloat(gc *GenConfiguration) (*[]float64, error) {
	if gc.Market != mp.Market || gc.Pair != mp.Name {
		return nil, fmt.Errorf("Wrong PairValue for market: %v   pair: %v", gc.Market, gc.Pair)
	}
	inputs := make([]float64, 0)

	indData, err := mp.toFloats(&gc.Indicators)
	if err == nil {
		inputs = append(inputs, indData...)

	} else {
		log.Printf("Err: %v\n", err.Error())
	}

	return &inputs, nil
}

func (f *PairValues) GetAllFloats() ([]float64, error) {
	result := []float64{}
	indis := "OB_EMAROC_10_P_10_idx3 OB_ROC_P_100_idx5 OB_EMAROC_20_P_10_idx14 RSI OB_ROC_P_100_idx14 OB_EMAROC_20_P_10_idx8 OB_ROC_P_1_idx13 OB_EMA_10_idx10 OB_EMAROC_20_P_10_idx0 OB_ROC_P_5_idx10 OB_ROC_P_1_0_idx14 OB_EMAROC_10_P_10_idx18 STOCH_OSC_D OB_EMAROC_5_P_10_idx10 OB_EMA_5_idx11 OB_EMAROC_5_P_10_idx17 OB_EMAROC_10_P_10_idx1 OB_EMAROC_20_P_10_idx3 OB_EMA_10_idx4 OB_ROC_P_5_idx13 OB_EMA_20_idx11 OB_ROC_P_100_idx1 OB_ROC_P_1_0_idx0 OB_EMAROC_20_P_10_idx18 OB_ROC_P_1_idx3 OB_EMA_5_idx19 OB_ROC_P_1_idx19 OB_ROC_P_1_idx14 OB_EMAROC_5_P_1_idx11 OB_EMAROC_5_P_10_idx18 OB_ROC_P_1_0_idx1 OB_ROC_P_1_0_idx3 TEMA OB_EMAROC_20_P_1_idx8 OB_EMAROC_5_P_10_idx3 OB_EMAROC_20_P_10_idx10 OB_EMA_5_idx6 OB_EMAROC_20_P_10_idx1 OB_EMAROC_10_P_10_idx7 OB_ROC_P_20_idx6 CCI OB_EMAROC_10_P_1_idx12 OB_ROC_P_1_idx0 OB_EMAROC_10_P_1_idx7 OB_ROC_P_1_0_idx2 VOLUME_ROC_P_5 OB_EMAROC_5_P_1_idx4 OB_EMAROC_10_P_10_idx6 OB_ROC_P_5_idx17 OB_EMA_5_idx2 AROON_DOWN OB_EMA_5_idx8 OB_ROC_P_1_idx12 OB_EMAROC_5_P_1_idx13 MARKETCAP_ROC_P_20 OB_EMA_20_idx5 OB_ROC_P_1_0_idx7 OB_EMAROC_5_P_10_idx15 MARKETCAP_ROC_P_5 OB_ROC_P_1_0_idx9 OB_EMAROC_10_P_10_idx17 AVAILABLE_SUPPLY_ROC_P_5 OB_ROC_P_100_idx12 OB_EMA_5_idx12 OB_EMAROC_10_P_10_idx16 OB_EMA_10_idx7 OB_EMAROC_20_P_10_idx19 OB_EMA_10_idx18 OB_ROC_P_5_idx18 OB_ROC_P_20_idx18 OB_EMAROC_20_P_1_idx10 OB_EMAROC_10_P_1_idx9 OB_EMA_20_idx13 OB_EMAROC_10_P_1_idx1 OB_EMAROC_20_P_10_idx12 OB_ROC_P_20_idx5 OB_ROC_P_1_0_idx19 OB_EMA_20_idx14 OB_EMAROC_20_P_10_idx13 ROC_R OB_EMAROC_20_P_1_idx7 OB_EMA_20_idx15 OB_ROC_P_20_idx7 OB_EMAROC_20_P_1_idx4 OB_EMAROC_10_P_10_idx0 OB_ROC_P_20_idx16 OB_EMAROC_10_P_10_idx5 OB_ROC_P_5_idx9 OB_EMA_10_idx5 OB_ROC_P_1_0_idx6 VOLUME_ROC_P_100 24HPC OB_EMA_10_idx9 OB_ROC_P_100_idx13 OB_ROC_P_1_0_idx11 OB_EMA_5_idx17 MACD_SIGNAL OB_EMA_10_idx1 OB_ROC_P_20_idx2 OB_ROC_P_1_0_idx8 OB_EMAROC_20_P_1_idx15 OB_EMAROC_20_P_10_idx16 OB_EMAROC_10_P_10_idx9 OB_ROC_P_100_idx6 OB_ROC_P_5_idx2 OB_EMAROC_10_P_10_idx13 OB_EMAROC_10_P_1_idx5 OB_EMA_20_idx8 OB_EMA_20_idx1 OB_ROC_P_20_idx0 OB_ROC_P_20_idx11 OB_EMAROC_10_P_1_idx10 OB_EMAROC_10_P_1_idx13 AVAILABLE_SUPPLY_ROC_P_20 OB_ROC_P_5_idx5 OB_EMAROC_10_P_1_idx19 OB_EMAROC_10_P_1_idx0 OB_EMA_10_idx6 OB_EMAROC_10_P_1_idx3 OB_EMA_5_idx5 OB_EMAROC_5_P_10_idx14 MARKETCAP_ROC_P_100 AVAILABLE_SUPPLY_ROC_P_10 OB_ROC_P_100_idx18 OB_EMAROC_5_P_1_idx15 OB_EMA_5_idx4 OB_ROC_P_1_0_idx5 OB_EMA_5_idx9 OB_ROC_P_5_idx3 OB_ROC_P_5_idx14 OB_EMAROC_20_P_1_idx3 OB_EMA_20_idx2 DEMA OB_ROC_P_1_0_idx13 OB_ROC_P_5_idx19 OB_ROC_P_1_0_idx15 OB_ROC_P_1_0_idx12 OB_ROC_P_5_idx0 OB_EMA_20_idx19 OB_ROC_P_20_idx10 OB_EMA_5_idx0 OB_EMAROC_10_P_1_idx2 OB_EMA_10_idx14 MACD_DIFF AROON_DIFF ROC_P TRIMA OB_ROC_P_100_idx2 OB_EMAROC_5_P_10_idx1 OB_ROC_P_5_idx1 OB_EMA_10_idx3 OB_EMAROC_20_P_10_idx2OB_EMAROC_5_P_1_idx0 OB_ROC_P_100_idx7 OB_EMAROC_5_P_10_idx13 OB_EMA_10_idx15 OB_ROC_P_5_idx11 ZRI OB_ROC_P_1_idx17 ADX OB_ROC_P_1_0_idx10 OB_ROC_P_100_idx10 OB_EMA_10_idx8 OB_ROC_P_20_idx1 BB_UB OB_EMAROC_5_P_1_idx7 OB_EMA_10_idx13 OB_EMA_20_idx17 OB_EMAROC_5_P_1_idx10 OB_EMAROC_10_P_1_idx8 OB_ROC_P_5_idx16 OB_EMAROC_5_P_10_idx8 OB_ROC_P_1_idx6 OB_ROC_P_1_0_idx4 OB_EMAROC_20_P_10_idx17 OB_EMA_5_idx13 OB_EMA_20_idx7 OB_EMAROC_5_P_1_idx6 OB_EMAROC_20_P_10_idx7 OB_EMAROC_5_P_1_idx16 OB_EMA_20_idx0 OB_EMA_10_idx19 OB_EMAROC_5_P_1_idx19 OB_EMA_5_idx16 OB_EMA_20_idx12 OB_ROC_P_1_idx16 OB_EMAROC_5_P_10_idx7 OB_EMAROC_10_P_10_idx10 OB_ROC_P_100_idx4 OB_ROC_P_1_idx8 OB_ROC_P_100_idx15 OB_EMA_10_idx17 OB_ROC_P_100_idx11 VOLUME_ROC_P_1 OB_ROC_P_5_idx8 OB_EMAROC_10_P_1_idx16 OB_ROC_P_5_idx15 MACD OB_EMA_5_idx3 OB_ROC_P_100_idx19 OB_ROC_P_1_0_idx17 OB_EMA_20_idx3 OB_EMAROC_20_P_10_idx11 OB_ROC_P_1_0_idx18 OB_ROC_P_20_idx4 OB_EMAROC_20_P_1_idx11 OB_EMAROC_5_P_1_idx14 STOCH_OSC_K OB_EMAROC_5_P_1_idx2 OB_EMA_20_idx6 OB_EMAROC_10_P_10_idx19 OB_EMAROC_20_P_1_idx5 OB_EMAROC_10_P_1_idx14 OB_EMAROC_10_P_1_idx6 OB_EMAROC_20_P_1_idx13 OB_EMAROC_5_P_10_idx9 OB_EMAROC_20_P_1_idx2 OB_ROC_P_5_idx7 OB_EMAROC_5_P_1_idx3 AVAILABLE_SUPPLY_ROC_P_1 OB_ROC_P_1_idx10 OB_ROC_P_1_0_idx16 OB_ROC_P_1_idx4 MARKETCAP_ROC_P_1 OB_EMAROC_20_P_10_idx6 OB_EMAROC_10_P_10_idx2 ROC_R_100 OB_ROC_P_20_idx15 OB_EMAROC_10_P_10_idx8 OB_EMA_5_idx7 OB_EMA_20_idx4 OB_EMAROC_20_P_10_idx9 AROON_UP OB_ROC_P_1_idx11 OB_EMAROC_5_P_1_idx12 OB_ROC_P_20_idx8 OB_ROC_P_1_idx7 OB_EMAROC_5_P_10_idx11 OB_ROC_P_100_idx17 OB_EMAROC_5_P_10_idx16 OB_EMA_10_idx2 VOLUME_ROC_P_10 OB_ROC_P_1_idx9 OB_EMAROC_20_P_1_idx12 OB_EMAROC_20_P_1_idx9 OB_ROC_P_20_idx14 OB_ROC_P_1_idx1 OB_EMA_20_idx10 OB_EMAROC_10_P_1_idx18 MFI OB_EMAROC_5_P_10_idx0 OB_ROC_P_100_idx0 OB_EMAROC_20_P_1_idx19 OB_EMAROC_10_P_10_idx11 AVAILABLE_SUPPLY_ROC_P_100 OB_ROC_P_100_idx16 OB_ROC_P_20_idx13 OB_EMA_5_idx14 OB_EMA_20_idx18 STOCH_RSI_K OB_EMA_5_idx1 OB_EMA_5_idx15 OB_EMAROC_20_P_10_idx4 OB_EMAROC_10_P_1_idx11 OB_ROC_P_1_idx15 ROC OB_ROC_P_5_idx4 OB_EMAROC_10_P_1_idx17 OB_EMAROC_10_P_10_idx14 OB_EMAROC_5_P_1_idx17 WILLR OB_EMAROC_5_P_10_idx2 MARKETCAP_ROC_P_10 OB_EMAROC_10_P_1_idx15 OB_EMA_10_idx16 OB_ROC_P_20_idx12 OB_EMAROC_20_P_1_idx16 OB_EMAROC_20_P_1_idx0 OB_ROC_P_20_idx3 OB_EMAROC_10_P_10_idx15 OB_EMA_10_idx11 7DPC OB_ROC_P_100_idx8 OB_ROC_P_5_idx6 OB_EMAROC_5_P_1_idx9 OB_EMA_20_idx9 SAR STOCH_RSI_D CHAIKIN_OSC OB_EMA_20_idx16 OB_EMA_5_idx10 OB_EMAROC_5_P_10_idx12 OB_ROC_P_100_idx3 OB_EMAROC_20_P_1_idx6 OB_EMAROC_10_P_10_idx12 OB_ROC_P_1_idx2 OB_ROC_P_1_idx5 OB_EMAROC_5_P_1_idx18 OB_ROC_P_1_idx18 OB_EMAROC_5_P_10_idx5 OB_ROC_P_20_idx17 OB_EMAROC_10_P_10_idx4 OB_EMAROC_5_P_10_idx19 OB_EMAROC_20_P_1_idx14 EMA50 DX OB_ROC_P_20_idx9 OB_ROC_P_100_idx9 VOLUME_ROC_P_20 OB_EMAROC_10_P_1_idx4 OB_EMAROC_20_P_1_idx18 OB_ROC_P_20_idx19 OB_ROC_P_5_idx12 OB_EMAROC_20_P_10_idx5 BB_LB OB_EMAROC_5_P_1_idx8 OB_EMAROC_5_P_1_idx5 1HPC OB_EMA_10_idx12 OB_EMAROC_5_P_10_idx4 OB_EMAROC_20_P_10_idx15 OB_EMA_5_idx18 OB_EMAROC_5_P_10_idx6 OB_EMAROC_5_P_1_idx1 BB_MB OB_EMAROC_20_P_1_idx17 OB_EMAROC_20_P_1_idx1 OB_EMA_10_idx0"
	indi := strings.Split(indis, " ")
	for _, indsName := range indi {

		//	fmt.Printf("(*f.Indicators)[indsName]=%v\n", (*f.Indicators)[indsName])

		if math.IsNaN((*f.Indicators)[indsName]) || math.IsInf((*f.Indicators)[indsName], 0) {
			message := fmt.Sprintf("NaN value for marker: %s:%s : %s\n", f.Market, f.Name, indsName)
			return nil, fmt.Errorf(message)
		}
		if vv, err := MapIndicator(f.Indicators, indsName, f.Value); err == nil {
			if vv > 1 || vv < 0 {
				log.Fatalf("Wrong map value:%s %v", indsName, vv)
			}
			//fmt.Printf("%v ", indsName)
			result = append(result, vv)

		}
	}
	return result, nil
}
func (f *PairValues) toFloats(indicators *[]string) ([]float64, error) {
	result := []float64{}

	for _, indsName := range *indicators {
		//	fmt.Printf("(*f.Indicators)[indsName]=%v\n", (*f.Indicators)[indsName])

		if math.IsNaN((*f.Indicators)[indsName]) || math.IsInf((*f.Indicators)[indsName], 0) {
			message := fmt.Sprintf("NaN value for marker: %s:%s : %s\n", f.Market, f.Name, indsName)
			return nil, fmt.Errorf(message)
		}
		if vv, err := MapIndicator(f.Indicators, indsName, f.Value); err == nil {
			if vv > 1 || vv < 0 {
				log.Fatalf("Wrong map value:%s %v", indsName, vv)
			}
			//log.Printf("Mapped: %v to %v\n", (*f.ValueIndicators)[indsName], vv)
			result = append(result, vv)
		}

	}

	return result, nil
	//return []float64{f.LowestAsk - f.HighestBid, f.PercentChange, f.LowestAskDelta30s, f.LowestAskPredictLinear, f.HighestBidDelta30s, f.HighestBidPredictLinear}
}

type TimeData struct {
	Id   int
	Time int64
	Data float64
}
