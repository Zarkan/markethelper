package markethelper

import (
	"fmt"
	"strings"

	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/thetruetrade/gotrade"
)

var (
	historyData = 50
	duration    = "-5s"
)

type PairTick struct {
	Market string
	Name   string
	Ask    float64
	Bid    float64
}

type PricePair struct {
	Pair  string
	Price float64
}
type BuyPair struct {
	NNID     uint `json:"NNID,string"`
	Src, Dst string
	PairTick PairTick
	Time     *time.Time
}

func (bp *BuyPair) String() string {
	return fmt.Sprintf("Src: %v	Dst: %v NNID: %v", bp.Src, bp.Dst, bp.NNID)
}

type Pair struct {
	Market         string
	Name           string
	Currency       string
	QuotaCurrency  string
	priceProm      prometheus.Gauge
	askProm        prometheus.Gauge
	bidProm        prometheus.Gauge
	valueIndicator *SingleValueIndicator
	askIndicator   *SingleValueIndicator
	bidIndicator   *SingleValueIndicator
}

func NewPair(market, name, currency, quotaCurrency string) *Pair {
	p := &Pair{
		Market:        market,
		Name:          name,
		Currency:      currency,
		QuotaCurrency: quotaCurrency,
	}
	p.valueIndicator = &SingleValueIndicator{Type: "value", Market: market, Name: name}
	p.bidIndicator = &SingleValueIndicator{Type: "bid", Market: market, Name: name}
	p.askIndicator = &SingleValueIndicator{Type: "ask", Market: market, Name: name}
	p.valueIndicator.Init()
	p.bidIndicator.Init()
	p.askIndicator.Init()

	p.init()

	return p
}
func (p *Pair) init() {

	p.priceProm, p.bidProm, p.askProm = initPromGauge(p.Market, p.Name)

}

type Indicators interface {
	ReceiveDOHLCVTick(tickData gotrade.DOHLCV, streamBarIndex int)
}

type AggregateIndicators interface {
	ReceiveTick(tickData float64, streamBarIndex int)
}

func initPromGauge(market, pair string) (price prometheus.Gauge, bid prometheus.Gauge, ask prometheus.Gauge) {
	pair = strings.Replace(strings.ToLower(pair), "-", "_", -1)
	promPrice := prometheus.NewGauge(prometheus.GaugeOpts{
		Name:      fmt.Sprintf("%v", pair),
		Help:      fmt.Sprintf("%s %s", market, pair),
		Subsystem: market,
	})

	promAsk := prometheus.NewGauge(prometheus.GaugeOpts{
		Name:      fmt.Sprintf("%v_ask", pair),
		Help:      fmt.Sprintf("%s ASK %s", market, pair),
		Subsystem: market,
	})

	promBid := prometheus.NewGauge(prometheus.GaugeOpts{
		Name:      fmt.Sprintf("%s_bid", pair),
		Help:      fmt.Sprintf("%s BID %s", market, pair),
		Subsystem: market,
	})

	prometheus.MustRegister(promPrice)
	prometheus.MustRegister(promBid)
	prometheus.MustRegister(promAsk)

	return promPrice, promBid, promAsk

}

//NewDOHLCVDataItem
func (p *Pair) Tick(pv *PairValues, calculate bool) {
	ts := time.Unix(0, pv.Time)
	//t0 := time.Now()
	p.priceProm.Set(pv.Value)
	p.askProm.Set(pv.LowestAsk)
	p.bidProm.Set(pv.HighestBid)
	//	pv.AskIndicators, pv.AskDohlcv = p.askIndicator.Tick(pv.LowestAsk, pv.BaseVolume, ts, 1)
	//	pv.BidIndicators, pv.BidDohlcv = p.bidIndicator.Tick(pv.HighestBid, pv.BaseVolume, ts, 2)
	if calculate {
		pv.Indicators, pv.Dohlcv = p.valueIndicator.Tick(pv.Value, pv.BaseVolume, ts, 3)
	} else {
		doh := DOHLCV{Date: pv.Time, OpenPrice: pv.Open, HighPrice: pv.HighestBid, LowPrice: pv.LowestAsk, ClosePrice: pv.Value, Volume: pv.BaseVolume}
		if doh.HighPrice == doh.LowPrice {
			doh.HighPrice = doh.LowPrice + 0.000001
		}
		pv.Indicators, pv.Dohlcv = p.valueIndicator.TickDOHLCV(doh, 3)
	}
	//t1 := time.Now()
	//log.Printf("### DOH: %v", pv.Dohlcv)

	//log.Printf("###################### Took: %v", t1.Sub(t0))

}

func (p *Pair) Next(pv *PairValues) {

	p.priceProm.Set(pv.Value)
	p.askProm.Set(pv.LowestAsk)
	p.bidProm.Set(pv.HighestBid)
	p.valueIndicator.Next(pv.Value)
	//t1 := time.Now()
	//log.Printf("###################### Took: %v", t1.Sub(ts))

}

func getDelta(tm *[]timeValue, duration time.Duration, currentTime time.Time, currentValue float64) float64 {

	timeCheck := currentTime.Add(duration)

	removeIdx := 0
	//log.Printf("\nBefore %v\n", *tm)
	for i, tv := range *tm {
		if tv.Time.Before(timeCheck) {
			removeIdx = i - 1
		} else {
			break
		}
	}
	if removeIdx > 0 {
		newtm := (*tm)[removeIdx+1:]
		//log.Printf("After idx: %v 		data: %v\n", removeIdx, newtm)
		tm = &newtm
		compareVal := newtm[0].Value
		//	fmt.Printf("foundTime: %v		timeCheck: %v\n", newtm[0].Time, timeCheck)
		return currentValue - compareVal
	}
	return 0
}

func SaveLast(x *[]float64) {

	if len(*x) > 1 {
		*x = (*x)[len(*x)-1:]
	}
}

func getMinMaxTM(x *[]timeValue) (float64, float64) {
	smallest, biggest := (*x)[0].Value, (*x)[0].Value
	for _, v := range *x {
		if v.Value > biggest {
			biggest = v.Value
		}
		if v.Value < smallest {
			smallest = v.Value
		}
	}
	return smallest, biggest
}

func getMinMax(x []float64) (float64, float64) {
	smallest, biggest := x[0], x[0]
	for _, v := range x {
		if v > biggest {
			biggest = v
		}
		if v < smallest {
			smallest = v
		}
	}
	return smallest, biggest
}
