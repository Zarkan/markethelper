package markethelper

import "time"

type Order struct {
	Id            string        `gorm:"primary_key"`
	Market        string        `gorm:"column:market"`
	ProductId     string        `gorm:"column:product_id"`
	OrgId         string        `gorm:"column:org_id"`
	OrgPrice      float64       `gormdb:"column:org_price"`
	OrgCurrency   string        `gorm:"column:org_currency"`
	SrcCurrency   string        `gorm:"column:src_currency"`
	DstCurrency   string        `gorm:"column:dst_currency"`
	CurrencyState CurrencyState `gorm:"column:currency_state"`
	Price         float64       `gorm:"column:price"`
	Size          float64       `gorm:"column:size"`
	Remains       float64       `gorm:"column:remains"`
	SourceId      string        `gorm:"column:source_id"` //wczesniejsze zam
	Mode          Mode          `gorm:"column:mode"`
	//CreatedAt exchange.Time
	Fee           float64 `gorm:"column:fee"`
	ExecutedValue float64 `gorm:"column:executed_value"`
	Status        Status  `gorm:"column:status"`
	ClosedBy      string  `gorm:"column:closed_by"`
	NNID          uint    `gorm:"column:nnid" json:",string"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
	done          chan struct{}
}

type Mode int

const ( // iota is reset to 0
	Normal Mode = iota // c0 == 0
	Aggressive
	Recover
)

type CurrencyState struct {
	Id      int     `gorm:"AUTO_INCREMENT"`
	OrderId string  `gorm:"column:order_id"`
	Pair    string  `gorm:"column:pair"`
	Ask     float64 `gorm:"column:ask"`
	Bid     float64 `gorm:"column:bid"`
}

type Status int

const ( // iota is reset to 0
	Open Status = iota // c0 == 0
	Filled
	Closed
	Canceled
)
