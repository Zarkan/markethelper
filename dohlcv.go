package markethelper

import (
	"fmt"
	"log"
	"time"
)

type DOHLCV struct {
	Date       int64
	OpenPrice  float64
	HighPrice  float64
	LowPrice   float64
	ClosePrice float64
	Volume     float64
}

func (d DOHLCV) String() string {
	return fmt.Sprintf("DOHC: D: %v, O: %f,H: %f,L: %f,C: %f, V: %f", d.D(), d.OpenPrice, d.HighPrice, d.LowPrice, d.ClosePrice, d.Volume)
}

func (d DOHLCV) D() time.Time {
	return time.Unix(0, d.Date)
}

func (d DOHLCV) O() float64 {
	return d.OpenPrice
}

func (d DOHLCV) H() float64 {
	return d.HighPrice
}

func (d DOHLCV) L() float64 {
	return d.LowPrice
}

func (d DOHLCV) C() float64 {
	return d.ClosePrice
}

func (d DOHLCV) V() float64 {
	return d.Volume
}

func GetDohlcvs(mps *[]*MarketValues, pair, market string) (*[]*DOHLCV, *[]*PairValues) {
	dohs := make([]*DOHLCV, 0, len(*mps))
	pvs := make([]*PairValues, 0, len(*mps))
	for _, mp := range *mps {
		pv, err := mp.GetPairValue(market, pair)
		if err != nil {
			log.Printf("Getting PairValue error: %v", err.Error())
		}
		dohs = append(dohs, pv.Dohlcv)
		pvs = append(pvs, pv)
	}
	return &dohs, &pvs
}
