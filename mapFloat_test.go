package markethelper

import "testing"

func TestAverage(t *testing.T) {
	var v float64
	v = MapFloatRange(0, -0.0001, 0.0001, 0, 1)
	if v != 0.5 {
		t.Error("Expected 1.5, got ", v)
	}
	v = MapFloatRange(50, 0, 100, 0, 1)
	if v != 0.5 {
		t.Error("Expected 1.5, got ", v)
	}
}
