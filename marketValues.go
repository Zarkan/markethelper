package markethelper

import (
	"fmt"
	"log"
	"math"
)

type MarketValues struct {
	Market string
	Time   int64
	Values *map[string]*PairValues
}

func (mv *MarketValues) GetPairValue(market, pair string) (*PairValues, error) {

	if mv.Market == market {
		if pv, ok := (*mv.Values)[pair]; ok {
			return pv, nil
		} else {
			return nil, fmt.Errorf("mp not for market: %s, pair: %s", market, pair)
		}
	} else {
		return nil, fmt.Errorf("mp not for market: %s", market)
	}

}

func (mp *MarketValues) GetFloat(gc *GenConfiguration) (*[]float64, error) {
	inputs := make([]float64, 0)

	indData, err := mp.getFloats(gc.Market, gc.Pair, &gc.Indicators)
	if err == nil {
		inputs = append(inputs, *indData...)

	} else {
		log.Printf("Err: %v\n", err.Error())
	}

	return &inputs, nil
}

func (mp *MarketValues) getFloats(market string, pair string, indicators *[]string) (*[]float64, error) {

	if mp.Market == market {
		result, err := mp.getFloatsByIds(market, pair, mp.Values, indicators)
		return result, err
	} else {
		return nil, fmt.Errorf("Market Value not for marker: %v", market)
	}

}

func (mp *MarketValues) getFloatsByIds(market string, pair string, allData *map[string]*PairValues, indicators *[]string) (*[]float64, error) {

	var result = make([]float64, 0)

	var err error
	var ff []float64
	if pairValue, ok := (*allData)[pair]; ok {
		ff, err = mp.toFloats(pairValue, market, indicators)
	} else {
		return nil, fmt.Errorf("No %s for %s", pair, market)
	}

	if err != nil {
		return nil, err
	}
	result = append(result, ff...)

	return &result, nil
}

func (mp *MarketValues) toFloats(f *PairValues, market string, indicators *[]string) ([]float64, error) {
	result := []float64{MapFloatRange(f.LowestAsk-f.HighestBid, -1, 1, 0, 1)}

	for _, indsName := range *indicators {
		//	fmt.Printf("(*f.Indicators)[indsName]=%v\n", (*f.Indicators)[indsName])

		if math.IsNaN((*f.Indicators)[indsName]) || math.IsInf((*f.Indicators)[indsName], 0) {
			message := fmt.Sprintf("NaN value for marker: %s:%s : %s\n", market, f.Name, indsName)
			return nil, fmt.Errorf(message)
		}
		if vv, err := MapIndicator(f.Indicators, indsName, f.Value); err == nil {

			if vv > 1 || vv < 0 {
				log.Fatalf("Wrong map value:%s %v", indsName, vv)
			}
			//log.Printf("Mapped: %v to %v\n", (*f.ValueIndicators)[indsName], vv)
			result = append(result, vv)
		}

	}

	return result, nil
	//return []float64{f.LowestAsk - f.HighestBid, f.PercentChange, f.LowestAskDelta30s, f.LowestAskPredictLinear, f.HighestBidDelta30s, f.HighestBidPredictLinear}
}
