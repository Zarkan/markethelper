package markethelper

import (
	"errors"
	"math"
	"strings"
)

/*
type MarketsPairs map[string]map[string]*PairValues

func (mp *MarketsPairs) GetPairValue(market string, pair string) (*PairValues, error) {
	if marketData, ok := (*mp)[market]; ok {
		if pairValue, ok := marketData[pair]; ok {
			return pairValue, nil
		} else {
			return nil, fmt.Errorf("No %s for %s", pair, market)
		}

	} else {
		return nil, fmt.Errorf("No data for %s", market)
	}

}

func (mp *MarketsPairs) GetFloat(gc *GenConfiguration) (*[]float64, error) {
	inputs := make([]float64, 0)
	for _, mc := range gc.Input {
		indData, err := mp.getFloats(mc.Market, &mc.Pairs, &mc.Indicators, &mc.IndicatorsType)
		if err == nil {
			inputs = append(inputs, *indData...)

		} else {
			log.Printf("Err: %v\n", err.Error())
		}

	}
	return &inputs, nil
}

func (mp *MarketsPairs) getFloats(market string, pairs *[]string, indicators, indicatorsType *[]string) (*[]float64, error) {

	marketData := (*mp)[market]
	result, err := mp.getFloatsByIds(market, pairs, &marketData, indicators, indicatorsType)
	return result, err

}

func (mp *MarketsPairs) getFloatsByIds(market string, pairs *[]string, allData *map[string]*PairValues, indicators) (*[]float64, error) {

	var result = make([]float64, 0)
	for _, pair := range *pairs {
		var err error
		var ff []float64
		if pairValue, ok := (*allData)[pair]; ok {
			ff, err = mp.toFloats(pairValue, market, indicators, indicatorsType)
		} else {
			return nil, fmt.Errorf("No %s for %s", pair, market)
		}

		if err != nil {
			return nil, err
		}
		result = append(result, ff...)
	}
	return &result, nil
}

func (mp *MarketsPairs) toFloats(f *PairValues, market string, indicators) ([]float64, error) {
	result := []float64{MapFloatRange(f.LowestAsk-f.HighestBid, -1, 1, 0, 1)}

	for _, indsName := range *indicators {
		//	fmt.Printf("(*f.Indicators)[indsName]=%v\n", (*f.Indicators)[indsName])

		if math.IsNaN((*f.Indicators)[indsName]) || math.IsInf((*f.Indicators)[indsName], 0) {
			message := fmt.Sprintf("NaN value for marker: %s:%s : %s\n", market, f.Name, indsName)
			return nil, fmt.Errorf(message)
		}
		vv := MapIndicator(f.Indicators, indsName)
		if vv > 1 || vv < 0 {
			log.Fatalf("Wrong map value:%s %v", indsName, vv)
		}
		//log.Printf("Mapped: %v to %v\n", (*f.ValueIndicators)[indsName], vv)
		result = append(result, vv)

	}
	return result, nil
	//return []float64{f.LowestAsk - f.HighestBid, f.PercentChange, f.LowestAskDelta30s, f.LowestAskPredictLinear, f.HighestBidDelta30s, f.HighestBidPredictLinear}
}
*/

func MapIndicator(data *map[string]float64, indicator string, value float64) (float64, error) {
	if indicator == "MACD" {
		return MapFloatRange((*data)[indicator], -1, 1, 0, 1), nil
	} else if indicator == "MACD_SIGNAL" {
		return MapFloatRange((*data)[indicator], -1, 1, 0, 1), nil
	} else if indicator == "MACD_DIFF" {
		return MapFloatRange((*data)[indicator], -0.0001, 0.0001, 0, 1), nil
	} else if indicator == "RSI" {
		return MapFloatRange((*data)[indicator], 0, 100, 0, 1), nil
	} else if indicator == "STOCH_OSC_K" {
		return MapFloatRange((*data)[indicator], 0, 100, 0, 1), nil
	} else if indicator == "STOCH_OSC_D" {
		return MapFloatRange((*data)[indicator], 0, 100, 0, 1), nil
	} else if indicator == "STOCH_RSI_K" {
		return MapFloatRange((*data)[indicator], 0, 100, 0, 1), nil
	} else if indicator == "STOCH_RSI_D" {
		return MapFloatRange((*data)[indicator], 0, 100, 0, 1), nil
	} else if indicator == "MFI" {
		return MapFloatRange((*data)[indicator], -1, 1, 0, 1), nil
	} else if indicator == "WILLR" {
		return MapFloatRange((*data)[indicator], -100, 0, 0, 1), nil
	} else if indicator == "DX" {
		return MapFloatRange((*data)[indicator], 0, 100, 0, 1), nil
	} else if indicator == "AROON_UP" {
		return MapFloatRange((*data)[indicator], 0, 100, 0, 1), nil
	} else if indicator == "AROON_DOWN" {
		return MapFloatRange((*data)[indicator], 0, 100, 0, 1), nil
	} else if indicator == "AROON_DIFF" {
		return MapFloatRange((*data)[indicator], -100, 100, 0, 1), nil
	} else if indicator == "ADX" {
		return MapFloatRange((*data)[indicator], 0, 100, 0, 1), nil
	} else if indicator == "DEMA" {
		return MapFloatRange((*data)[indicator], 0, 1, 0, 1), nil
	} else if indicator == "DELTA5S" {
		return MapFloatRange((*data)[indicator], -1, 1, 0, 1), nil
	} else if indicator == "BB_UB" {
		if (*data)[indicator] < value {
			return 1, nil
		} else {
			return 0, nil
		}
	} else if indicator == "BB_MB" {
		if (*data)[indicator] < value {
			return 1, nil
		} else {
			return 0, nil
		}
	} else if indicator == "BB_LB" {
		if (*data)[indicator] > value {
			return 1, nil
		} else {
			return 0, nil
		}
	} else if indicator == "SAR" {
		if (*data)[indicator] > value {
			return 1, nil
		} else {
			return 0, nil
		}
	} else if indicator == "CCI" {
		if (*data)[indicator] >= 100 {
			return 1, nil
		} else if (*data)[indicator] < 100 && (*data)[indicator] > -100 {
			return MapFloatRange((*data)[indicator], -100, 100, 0.1, 0.9), nil
		} else {
			return 0, nil
		}
	} else if indicator == "CHAIKIN_OSC" {
		if (*data)[indicator] > 0 {
			return 1, nil
		} else {
			return 0, nil
		}
	} else if indicator == "ROC" {
		if (*data)[indicator] > 0 {
			return 1, nil
		} else {
			return 0, nil
		}
	} else if indicator == "WILLR" {
		return MapFloatRange((*data)[indicator], -100, 0, 0, 1), nil
	} else if indicator == "ROC_P" {
		return MapFloatRange((*data)[indicator], -0.1, 0.1, 0, 1), nil
	} else if indicator == "EMA50" {
		if (*data)[indicator] > value {
			return 1, nil
		} else {
			return 0, nil
		}

	} else if indicator == "TEMA" {
		if (*data)[indicator] > value {
			return 1, nil
		} else {
			return 0, nil
		}
	} else if indicator == "TRIMA" {
		if (*data)[indicator] > value {
			return 1, nil
		} else {
			return 0, nil
		}
	} else if strings.Contains(indicator, "ZRI") {
		if (*data)[indicator] > 0 {
			return 1, nil
		} else {
			return 0, nil
		}
	} else if strings.Contains(indicator, "1HPC") {
		return MapFloatRange((*data)[indicator], -100, 100, 0.1, 0.9), nil
	} else if strings.Contains(indicator, "24HPC") {
		return MapFloatRange((*data)[indicator], -100, 100, 0.1, 0.9), nil
	} else if strings.Contains(indicator, "7DPC") {
		return MapFloatRange((*data)[indicator], -100, 100, 0.1, 0.9), nil
	} else if strings.Contains(indicator, "ROC") {
		if (*data)[indicator] > 100 {
			return 1, nil
		} else if (*data)[indicator] < -100 {
			return 0, nil
		} else {
			return MapFloatRange((*data)[indicator], -100, 100, 0.1, 0.9), nil
		}
	} else if strings.Contains(indicator, "OB_EMA") {
		if (*data)[indicator] > 0 {
			return 1, nil
		} else {
			return 0, nil
		}
	}

	return 0, errors.New("No mapping")
}

/*
} */

func MapFloatRange(value, inMin, inMax, outMin, outMax float64) float64 {
	result := (value-inMin)*(outMax-outMin)/(inMax-inMin) + outMin
	return math.Max(outMin, math.Min(result, outMax))
}
