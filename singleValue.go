package markethelper

import (
	"log"
	"math"
	"time"

	"github.com/thetruetrade/gotrade"
	"github.com/thetruetrade/gotrade/indicators"
)

type SingleValueIndicator struct {
	Market string
	Name   string
	Type   string

	lastMapIndicator   *map[string]float64
	indicators         map[string]Indicators
	timeValues         []timeValue
	deltaTimeValues    []timeValue
	tempList           []float64
	tt                 int
	deltaDuration      time.Duration
	indicatorsDuration time.Duration
	min, max, open     float64
	initRange          bool
}

func (s *SingleValueIndicator) Init() {
	s.initRange = true
	var err error
	if s.deltaDuration, err = time.ParseDuration(duration); err != nil {
		log.Println("ERROR Parse duration issue: %v", err.Error())
	}
	if s.indicatorsDuration, err = time.ParseDuration("-3m"); err != nil {
		log.Println("ERROR Parse duration issue: %v", err.Error())
	}

	s.initIndicators()
}
func (p *SingleValueIndicator) setIndicator(key string, vals *[]float64, resultMap *map[string]float64) {
	SaveLast(vals)
	val := (*vals)[len(*vals)-1]

	if math.IsNaN(val) || math.IsInf(val, 0) {
		log.Printf("INVALID: key: %s,	val:%v\n", key, val)
		if p.lastMapIndicator != nil {
			if lastVal, ok := (*p.lastMapIndicator)[key]; ok && !math.IsNaN(lastVal) {
				val = lastVal
			} else {
				val = 0
			}

		} else {
			val = 0
		}
	}

	(*resultMap)[key] = val
}

func SetIndicator(key string, vals *[]float64, resultMap *map[string]float64) {
	SaveLast(vals)
	val := (*vals)[len(*vals)-1]

	if math.IsNaN(val) || math.IsInf(val, 0) {
		return
	}

	(*resultMap)[key] = val
}

func (p *SingleValueIndicator) Next(val float64) {
	if p.initRange {
		p.initRange = false
		p.open = val
		p.max = val
		p.min = val
	} else {
		if val > p.max {
			p.max = val
		} else if val < p.min {
			p.min = val
		}
	}

}

func (p *SingleValueIndicator) Tick(val, volume float64, ts time.Time, stream int) (*map[string]float64, *DOHLCV) {
	p.initRange = true
	if p.max == p.min {
		p.max = p.min + 0.000001
	}
	doh := DOHLCV{Date: ts.UnixNano(), OpenPrice: p.open, HighPrice: p.max, LowPrice: p.min, ClosePrice: val, Volume: volume}
	return p.TickDOHLCV(doh, stream)
}
func (p *SingleValueIndicator) TickDOHLCV(doh DOHLCV, stream int) (*map[string]float64, *DOHLCV) {

	//di := gotrade.NewDOHLCVDataItem(ts, doh.OpenPrice, doh.HighPrice, doh.LowPrice, doh.ClosePrice, volume)

	//log.Printf("Dohlcv: %v\n", pv.Dohlcv)
	for _, indicator := range p.indicators {
		indicator.ReceiveDOHLCVTick(doh, stream)
	}
	missing := make([]string, 0, 0)

	resultMap := make(map[string]float64)

	if so, ok := p.indicators["MACD"].(*indicators.Macd); ok {
		if len(so.Macd) > 1 && len(so.Signal) > 1 && len(so.Histogram) > 1 {
			p.setIndicator("MACD", &so.Macd, &resultMap)
			p.setIndicator("MACD_SIGNAL", &so.Signal, &resultMap)
			p.setIndicator("MACD_DIFF", &so.Histogram, &resultMap)
		} else {
			missing = append(missing, "MACD")
		}
		//log.Printf("MACD: Macd: %v	Signal: %v	Histogram:%v\n", so.Macd, so.Signal, so.Histogram)
	}
	if so, ok := p.indicators["RSI"].(*indicators.Rsi); ok {
		if len(so.Data) > 1 {
			p.setIndicator("RSI", &so.Data, &resultMap)

		} else {
			missing = append(missing, "RSI")
		}
	}
	if so, ok := p.indicators["STOCH_OSC"].(*indicators.StochOsc); ok {

		if len(so.SlowK) > 1 && len(so.SlowK) > 1 {
			p.setIndicator("STOCH_OSC_K", &so.SlowK, &resultMap)
			p.setIndicator("STOCH_OSC_D", &so.SlowD, &resultMap)
		} else {
			missing = append(missing, "STOCH_OSC")
		}

	}
	if so, ok := p.indicators["STOCH_RSI"].(*indicators.StochRsi); ok {
		if len(so.SlowK) > 1 && len(so.SlowK) > 1 {
			p.setIndicator("STOCH_RSI_K", &so.SlowK, &resultMap)
			p.setIndicator("STOCH_RSI_D", &so.SlowD, &resultMap)
		} else {
			missing = append(missing, "STOCH_RSI")
		}
		//	log.Printf("STOCH_RSI: sk: %v	sd: %v\n", so.SlowK, so.SlowD)
	}

	if so, ok := p.indicators["MFI"].(*indicators.Mfi); ok {
		if len(so.Data) > 1 {
			p.setIndicator("MFI", &so.Data, &resultMap)
			//	log.Printf("MFI: data: %v	\n", so.Data)
		} else {
			missing = append(missing, "MFI")
		}
	}
	if so, ok := p.indicators["WILLR"].(*indicators.WillR); ok {
		if len(so.Data) > 1 {
			p.setIndicator("WILLR", &so.Data, &resultMap)
			//	log.Printf("WillR: data: %v	\n", so.Data)
		} else {
			missing = append(missing, "WILLR")
		}
	}
	if so, ok := p.indicators["DX"].(*indicators.Dx); ok {
		if len(so.Data) > 1 {
			p.setIndicator("DX", &so.Data, &resultMap)
			//	log.Printf("Dx: data: %v	\n", so.Data)
		}
	}
	if so, ok := p.indicators["AROON"].(*indicators.Aroon); ok {
		if len(so.Up) > 1 && len(so.Down) > 1 {
			p.setIndicator("AROON_UP", &so.Up, &resultMap)
			p.setIndicator("AROON_DOWN", &so.Down, &resultMap)

			resultMap["AROON_DIFF"] = so.Up[0] - so.Down[0]
		} else {
			missing = append(missing, "AROON")
		}
		//	log.Printf("AROON: up: %v	down: %v\n", so.Up, so.Down)
	}
	if so, ok := p.indicators["ADX"].(*indicators.Adx); ok {
		if len(so.Data) > 1 {
			p.setIndicator("ADX", &so.Data, &resultMap)
		} else {
			missing = append(missing, "ADX")
		}
		//	log.Printf("ADX: data %v\n", so.Data)
	}
	//usless
	if so, ok := p.indicators["DEMA"].(*indicators.Dema); ok {
		if len(so.Data) > 1 {
			p.setIndicator("DEMA", &so.Data, &resultMap)
		} else {
			missing = append(missing, "DEMA")
		}
	}

	if so, ok := p.indicators["EMA25"].(*indicators.Ema); ok {
		if len(so.Data) > 1 {
			p.setIndicator("EMA25", &so.Data, &resultMap)

			if so1, ok := p.indicators["EMA25_ROC_P_1"].(*indicators.Ema); ok {
				so1.ReceiveTick(so.Data[0], 3)
				if len(so1.Data) > 1 {
					p.setIndicator("EMA25_ROC_P_1", &so1.Data, &resultMap)
				} else {
					missing = append(missing, "EMA25_ROC_P_1")
				}
			}

		} else {
			missing = append(missing, "EMA25")
		}
	}

	if so, ok := p.indicators["EMA50"].(*indicators.Ema); ok {
		if len(so.Data) > 1 {
			p.setIndicator("EMA50", &so.Data, &resultMap)
			if so1, ok := p.indicators["EMA50_ROC_P_1"].(*indicators.Ema); ok {
				so1.ReceiveTick(so.Data[0], 3)
				if len(so1.Data) > 1 {
					p.setIndicator("EMA50_ROC_P_1", &so1.Data, &resultMap)
				} else {
					missing = append(missing, "EMA50_ROC_P_1")
				}
			}

		} else {
			missing = append(missing, "EMA50")
		}
	}

	if so, ok := p.indicators["EMA100"].(*indicators.Ema); ok {
		if len(so.Data) > 1 {
			p.setIndicator("EMA100", &so.Data, &resultMap)
			if so1, ok := p.indicators["EMA100_ROC_P_1"].(*indicators.Ema); ok {
				so1.ReceiveTick(so.Data[0], 3)
				if len(so1.Data) > 1 {
					p.setIndicator("EMA100_ROC_P_1", &so1.Data, &resultMap)
				} else {
					missing = append(missing, "EMA100_ROC_P_1")
				}
			}

		} else {
			missing = append(missing, "EMA100")
		}
	}

	if so, ok := p.indicators["EMA150"].(*indicators.Ema); ok {
		if len(so.Data) > 1 {
			p.setIndicator("EMA150", &so.Data, &resultMap)
			if so1, ok := p.indicators["EMA150_ROC_P_1"].(*indicators.Ema); ok {
				so1.ReceiveTick(so.Data[0], 3)
				if len(so1.Data) > 1 {
					p.setIndicator("EMA150_ROC_P_1", &so1.Data, &resultMap)
				} else {
					missing = append(missing, "EMA150_ROC_P_1")
				}
			}

		} else {
			missing = append(missing, "EMA150")
		}
	}

	if so, ok := p.indicators["BB"].(*indicators.BollingerBands); ok {
		if len(so.UpperBand) > 1 && len(so.MiddleBand) > 1 && len(so.LowerBand) > 1 {
			p.setIndicator("BB_UB", &so.UpperBand, &resultMap)
			p.setIndicator("BB_MB", &so.MiddleBand, &resultMap)
			p.setIndicator("BB_LB", &so.LowerBand, &resultMap)
		} else {
			missing = append(missing, "BB")
		}
	}

	if so, ok := p.indicators["KAMA"].(*indicators.Kama); ok {
		if len(so.Data) > 1 {
			p.setIndicator("KAMA", &so.Data, &resultMap)
		} else {
			missing = append(missing, "KAMA")
		}
	}

	if so, ok := p.indicators["CCI"].(*indicators.Cci); ok {
		if len(so.Data) > 1 {
			p.setIndicator("CCI", &so.Data, &resultMap)
		} else {
			missing = append(missing, "CCI")
		}
	}

	if so, ok := p.indicators["CHAIKIN_OSC"].(*indicators.ChaikinOsc); ok {
		if len(so.Data) > 1 {
			p.setIndicator("CHAIKIN_OSC", &so.Data, &resultMap)
		} else {
			missing = append(missing, "CHAIKIN_OSC")
		}
	}

	if so, ok := p.indicators["AROON_OSC"].(*indicators.AroonOsc); ok {
		if len(so.Data) > 1 {
			p.setIndicator("AROON_OSC", &so.Data, &resultMap)
		} else {
			missing = append(missing, "AROON_OSC")
		}
	}

	if so, ok := p.indicators["HHV"].(*indicators.Hhv); ok {
		if len(so.Data) > 1 {
			p.setIndicator("HHV", &so.Data, &resultMap)
		} else {
			missing = append(missing, "HHV")
		}
	}

	if so, ok := p.indicators["HHV_BARS"].(*indicators.HhvBars); ok {
		if len(so.Data) > 1 {

			if len(so.Data) > 1 {
				so.Data = (so.Data)[len(so.Data)-1:]
			}
			v := float64(so.Data[0])
			resultMap["HHV_BARS"] = v
		} else {
			missing = append(missing, "HHV_BARS")
		}
	}

	if so, ok := p.indicators["ATR"].(*indicators.Atr); ok {
		if len(so.Data) > 1 {
			p.setIndicator("ATR", &so.Data, &resultMap)
		} else {
			missing = append(missing, "ATR")
		}
	}

	if so, ok := p.indicators["LLV_BARS"].(*indicators.LlvBars); ok {
		if len(so.Data) > 1 {
			if len(so.Data) > 1 {
				so.Data = (so.Data)[len(so.Data)-1:]
			}
			v := float64(so.Data[0])
			resultMap["LLV_BARS"] = v
		} else {
			missing = append(missing, "LLV_BARS")
		}
	}

	if so, ok := p.indicators["LLV"].(*indicators.Llv); ok {
		if len(so.Data) > 1 {
			p.setIndicator("LLV", &so.Data, &resultMap)
		} else {
			missing = append(missing, "LLV")
		}
	}

	if so, ok := p.indicators["MINUS_DI"].(*indicators.MinusDi); ok {
		if len(so.Data) > 1 {
			p.setIndicator("MINUS_DI", &so.Data, &resultMap)
		} else {
			missing = append(missing, "MINUS_DI")
		}
	}

	if so, ok := p.indicators["MINUS_DM"].(*indicators.MinusDm); ok {
		if len(so.Data) > 1 {
			p.setIndicator("MINUS_DM", &so.Data, &resultMap)
		} else {
			missing = append(missing, "MINUS_DM")
		}
	}

	if so, ok := p.indicators["MOM"].(*indicators.Mom); ok {
		if len(so.Data) > 1 {
			p.setIndicator("MOM", &so.Data, &resultMap)
		} else {
			missing = append(missing, "MOM")
		}
	}

	if so, ok := p.indicators["PLUS_DI"].(*indicators.PlusDi); ok {
		if len(so.Data) > 1 {
			p.setIndicator("PLUS_DI", &so.Data, &resultMap)
		} else {
			missing = append(missing, "PLUS_DI")
		}
	}

	if so, ok := p.indicators["PLUS_DM"].(*indicators.PlusDm); ok {
		if len(so.Data) > 1 {
			p.setIndicator("PLUS_DM", &so.Data, &resultMap)
		} else {
			missing = append(missing, "PLUS_DM")
		}
	}

	if so, ok := p.indicators["ROC"].(*indicators.Roc); ok {
		if len(so.Data) > 1 {
			p.setIndicator("ROC", &so.Data, &resultMap)
		} else {
			missing = append(missing, "ROC")
		}
	}

	if so, ok := p.indicators["ROC_P"].(*indicators.RocP); ok {
		if len(so.Data) > 1 {
			p.setIndicator("ROC_P", &so.Data, &resultMap)
		} else {
			missing = append(missing, "ROC_P")
		}
	}

	if so, ok := p.indicators["ROC_R"].(*indicators.RocR); ok {
		if len(so.Data) > 1 {
			p.setIndicator("ROC_R", &so.Data, &resultMap)
		} else {
			missing = append(missing, "ROC_R")
		}
	}

	if so, ok := p.indicators["ROC_R_100"].(*indicators.RocR100); ok {
		if len(so.Data) > 1 {
			p.setIndicator("ROC_R_100", &so.Data, &resultMap)
		} else {
			missing = append(missing, "ROC_R_100")
		}
	}

	if so, ok := p.indicators["SAR"].(*indicators.Sar); ok {
		if len(so.Data) > 1 {
			p.setIndicator("SAR", &so.Data, &resultMap)
		} else {
			missing = append(missing, "SAR")
		}
	}

	if so, ok := p.indicators["SMA"].(*indicators.Sma); ok {
		if len(so.Data) > 1 {
			p.setIndicator("SMA", &so.Data, &resultMap)
		} else {
			missing = append(missing, "SMA")
		}
	}

	/*if so, ok := p.indicators["STD_DEV"].(*indicators.StdDev); ok {
		if len(so.Data) > 1 {
			p.setIndicator("STD_DEV", &so.Data, &resultMap)
		} else {
			missing = append(missing, "STD_DEV")
		}
	}*/

	if so, ok := p.indicators["TEMA"].(*indicators.Tema); ok {
		if len(so.Data) > 1 {
			p.setIndicator("TEMA", &so.Data, &resultMap)
		} else {
			missing = append(missing, "TEMA")
		}
	}

	if so, ok := p.indicators["TRIMA"].(*indicators.Trima); ok {
		if len(so.Data) > 1 {
			p.setIndicator("TRIMA", &so.Data, &resultMap)
		} else {
			missing = append(missing, "TRIMA")
		}
	}

	if so, ok := p.indicators["TSF"].(*indicators.Tsf); ok {
		if len(so.Data) > 1 {
			p.setIndicator("TSF", &so.Data, &resultMap)
		} else {
			missing = append(missing, "TSF")
		}
	}

	if so, ok := p.indicators["VAR"].(*indicators.Var); ok {
		if len(so.Data) > 1 {
			p.setIndicator("VAR", &so.Data, &resultMap)
		} else {
			missing = append(missing, "VAR")
		}
	}

	if so, ok := p.indicators["WMA"].(*indicators.Wma); ok {
		if len(so.Data) > 1 {
			p.setIndicator("WMA", &so.Data, &resultMap)
		} else {
			missing = append(missing, "WMA")
		}
	}

	p.lastMapIndicator = &resultMap

	if len(missing) == 0 {
		return &resultMap, &doh
	} else {

		for _, key := range missing {

			log.Printf("Missing key: %v", key)

		}
	}

	return nil, nil
}

type timeValue struct {
	Time  time.Time
	Value float64
}

func (p *SingleValueIndicator) initIndicators() {
	p.indicators = make(map[string]Indicators)

	p.indicators["MACD"], _ = indicators.NewDefaultMacd() //c

	p.indicators["RSI"], _ = indicators.NewDefaultRsi()

	p.indicators["STOCH_OSC"], _ = indicators.NewStochOsc(14, 2, 4) //ReceiveDOHLCVTick H,L

	p.indicators["STOCH_RSI"], _ = indicators.NewDefaultStochRsi() //ReceiveDOHLCVTick C

	p.indicators["MFI"], _ = indicators.NewDefaultMfi() //ReceiveDOHLCVTick V

	p.indicators["WILLR"], _ = indicators.NewDefaultWillR() //ReceiveDOHLCVTickH,L,C

	p.indicators["DX"], _ = indicators.NewDefaultDx() //ReceiveDOHLCVTick, H,L,C

	p.indicators["AROON"], _ = indicators.NewDefaultAroon() //ReceiveDOHLCVTick H,L

	p.indicators["ADX"], _ = indicators.NewDefaultAdx()

	p.indicators["DEMA"], _ = indicators.NewDefaultDema() //C

	p.indicators["EMA25"], _ = indicators.NewDefaultEma() //C

	p.indicators["EMA25_ROC_P_1"], _ = indicators.NewRocP(1, gotrade.UseClosePrice) //C

	p.indicators["EMA50"], _ = indicators.NewEma(50, gotrade.UseClosePrice) //C

	p.indicators["EMA50_ROC_P_1"], _ = indicators.NewRocP(1, gotrade.UseClosePrice) //C

	p.indicators["EMA100"], _ = indicators.NewEma(100, gotrade.UseClosePrice) //C

	p.indicators["EMA100_ROC_P_1"], _ = indicators.NewRocP(1, gotrade.UseClosePrice) //C

	p.indicators["EMA150"], _ = indicators.NewEma(150, gotrade.UseClosePrice) //C

	p.indicators["EMA150_ROC_P_1"], _ = indicators.NewRocP(1, gotrade.UseClosePrice) //C

	p.indicators["BB"], _ = indicators.NewBollingerBands(20, gotrade.UseClosePrice) //C

	p.indicators["KAMA"], _ = indicators.NewDefaultKama() //C

	p.indicators["CCI"], _ = indicators.NewDefaultCci() //Oscilator >100 lub <-100

	p.indicators["CHAIKIN_OSC"], _ = indicators.NewDefaultChaikinOsc() //Oscilator >0 lub <0

	p.indicators["AROON_OSC"], _ = indicators.NewDefaultAroonOsc() //

	p.indicators["HHV"], _ = indicators.NewDefaultHhv() //Do wywalenia

	p.indicators["HHV_BARS"], _ = indicators.NewDefaultHhvBars() //Do wywalenia

	p.indicators["ATR"], _ = indicators.NewDefaultAtr() //Nie czaje

	p.indicators["LLV_BARS"], _ = indicators.NewDefaultLlvBars() //Do wywalenia

	p.indicators["LLV"], _ = indicators.NewDefaultLlv() //Do wywalenia

	p.indicators["MINUS_DI"], _ = indicators.NewDefaultMinusDi() //powiazane z atr

	p.indicators["MINUS_DM"], _ = indicators.NewDefaultMinusDm() //powiazane z atr

	p.indicators["MOM"], _ = indicators.NewDefaultMom() //jak price, bez sensu

	p.indicators["PLUS_DI"], _ = indicators.NewDefaultPlusDi() //powiazane z atr

	p.indicators["PLUS_DM"], _ = indicators.NewDefaultPlusDm() //powiazane z atr

	p.indicators["ROC"], _ = indicators.NewDefaultRoc() //C

	p.indicators["ROC_P"], _ = indicators.NewDefaultRocP() //C

	p.indicators["ROC_P_1"], _ = indicators.NewRocP(1, gotrade.UseClosePrice) //C

	p.indicators["ROC_R"], _ = indicators.NewDefaultRocR() //C

	p.indicators["ROC_R_100"], _ = indicators.NewDefaultRocR100() //C

	p.indicators["SAR"], _ = indicators.NewDefaultSar() //C

	p.indicators["SMA"], _ = indicators.NewDefaultSma()

	//	p.indicators["STD_DEV"], _ = indicators.NewDefaultStdDev()

	p.indicators["TEMA"], _ = indicators.NewDefaultTema()

	p.indicators["TRIMA"], _ = indicators.NewDefaultTrima()

	p.indicators["TSF"], _ = indicators.NewDefaultTsf()

	p.indicators["VAR"], _ = indicators.NewDefaultVar()

	p.indicators["WMA"], _ = indicators.NewDefaultWma()

}
