package markethelper

import (
	"time"
)

type OrderB struct {
	Quantity float64 `json:"Quantity"`
	Rate     float64 `json:"Rate"`
}

type CurrentOrderBook struct {
	Market     string
	Pair       string
	Buy        []OrderB
	Sell       []OrderB
	Price      float64
	Time       time.Time
	Spread     float64
	Indicators map[string]float64
}
